﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FreeNet;

namespace CSampleServer
{
	class Program
	{
		static List<CGameUser> userlist;

        static void Main(string[] args)
		{
            Console.CancelKeyPress += new ConsoleCancelEventHandler(Ctrl_C_Pressed);            

            CPacketBufferManager.initialize(2000);
			userlist = new List<CGameUser>();
            //접속 유저를 처리할 월드맵 로직
            WorldLogic.Instance.userlist = userlist;

            CNetworkService service = new CNetworkService();
			// 콜백 매소드 설정.
			service.session_created_callback += on_session_created;
			// 초기화.
			service.initialize();
			service.listen("0.0.0.0", 7979, 100);




            Console.WriteLine("Started!");
			while (true)
			{
                //서버의 메인 로직
                WorldLogic.Instance.Update();

                //0.1초마다 업데이트 로직이 돈다.
                //System.Threading.Thread.Sleep(100);
                //for(int i = 0; i < userlist.Count; ++i)
                //{
                //    CPacket response = CPacket.create((short)GameServer.PROTOCOL.CHAT_MSG_ACK);
                //    response.push("Hello : "+i);
                //    userlist[i].send(response);
                //}
			}
			Console.ReadKey();
		}

        private static void Ctrl_C_Pressed(object sender, ConsoleCancelEventArgs e)
        {
            WorldLogic.Instance.SaveAllDic();
            Environment.Exit(0);
        }

        /// <summary>
        /// 클라이언트가 접속 완료 하였을 때 호출됩니다.
        /// n개의 워커 스레드에서 호출될 수 있으므로 공유 자원 접근시 동기화 처리를 해줘야 합니다.
        /// </summary>
        /// <returns></returns>
        static void on_session_created(CUserToken token)
		{
			CGameUser user = new CGameUser(token);
			lock (userlist)
			{
				userlist.Add(user);
			}
		}

		public static void remove_user(CGameUser user)
		{
			lock (userlist)
			{
				userlist.Remove(user);
			}
		}
	}
}
