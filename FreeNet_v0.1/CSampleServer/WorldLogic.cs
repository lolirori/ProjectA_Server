﻿using System;
using System.Collections.Generic;
using SimpleJSON;

namespace CSampleServer
{
    using FreeNet;
    using GameProtocol;
    using Newtonsoft.Json;
    using System.IO;
    using System.Runtime.Serialization.Formatters.Binary;
    using System.Xml.Serialization;
    using UnityEngine;

    /// <summary>
    /// World에 접속해 있는 유저들의 정보 공유, 입력 공유를 한다.
    /// </summary>
    public class WorldLogic
    {
        //id, password
        Dictionary<string, string> dicLogin = new Dictionary<string, string>();
        static WorldLogic _instance;
        public static WorldLogic Instance
        {
            get
            {
                if (_instance == null)
                    _instance = new WorldLogic();
                return _instance;
            }
        }

        public WorldLogic()
        {
            _instance = this;
            dicLogin = Deserialize<Dictionary<string, string>>(File.Open("C:\\Users\\이용선\\Desktop\\TempDB\\Logins.bin", FileMode.Open));
        }

        public void SaveAllDic()
        {
            Serialize(dicLogin, File.Open("C:\\Users\\이용선\\Desktop\\TempDB\\Logins.bin", FileMode.Create));
        }

        public List<CGameUser> userlist;    

        public bool LoginCheck(string id, string pw)
        {
            if(dicLogin.ContainsKey(id) && dicLogin[id] == pw)
            {
                return true;
            }
                
            return false;
        }

        /// <summary>
        /// 입력의 공유
        /// </summary>
        public void Update()
        {            

        }

        public void SendPacketNear(CPacket packet, CGameUser withOut = null)
        {
            for (int i = 0; i < userlist.Count; ++i)
                if(withOut != userlist[i] && userlist[i].IsInWorld)
                    userlist[i].send(packet);
        }


        public static void Serialize<Object>(Object dictionary, Stream stream)
        {
            try // try to serialize the collection to a file
            {
                using (stream)
                {
                    // create BinaryFormatter
                    BinaryFormatter bin = new BinaryFormatter();
                    // serialize the collection (EmployeeList1) to file (stream)
                    bin.Serialize(stream, dictionary);
                }
            }
            catch (IOException)
            {
            }
        }

        public static Object Deserialize<Object>(Stream stream) where Object : new()
        {
            Object ret = CreateInstance<Object>();
            try
            {
                using (stream)
                {
                    // create BinaryFormatter
                    BinaryFormatter bin = new BinaryFormatter();
                    // deserialize the collection (Employee) from file (stream)
                    ret = (Object)bin.Deserialize(stream);
                }
            }
            catch (IOException)
            {
            }
            return ret;
        }
        // function to create instance of T
        public static Object CreateInstance<Object>() where Object : new()
        {
            return (Object)Activator.CreateInstance(typeof(Object));
        }
    }
}