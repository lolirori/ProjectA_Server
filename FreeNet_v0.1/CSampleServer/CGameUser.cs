﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FreeNet;
using Newtonsoft.Json;

namespace CSampleServer
{
	using GameProtocol;
    using Newtonsoft.Json;
    using System.IO;
    using System.Runtime.Serialization.Formatters.Binary;
    using UnityEngine;



    /// <summary>
    /// 하나의 session객체를 나타낸다.
    /// </summary>
    public class CGameUser : IPeer
	{
        public bool IsCanSend = true;

        CUserToken token;
        static int currentId = 1;

        public string playerStateJson = "";
        public int playerInstanceId = 0;

        DateTime lastRecive;
        float intervalNetwork;

        public bool IsInWorld { get; private set; }

        public CGameUser(CUserToken token)
		{
			this.token = token;
			this.token.set_peer(this);
		}
        void IPeer.on_message(Const<byte[]> buffer)
		{
			// ex)
			CPacket msg = new CPacket(buffer.Value, this);
			PROTOCOL protocol = (PROTOCOL)msg.pop_protocol_id();
			Console.WriteLine("------------------------------------------------------");
			Console.WriteLine("protocol id " + protocol);
			switch (protocol)
			{
                case PROTOCOL.APP_UP:
                    {
                        IsCanSend = true;
                        break;
                    }
                case PROTOCOL.APP_DOWN:
                    {
                        IsCanSend = false;
                        break;
                    }


                case PROTOCOL.OBJ_NEW:
                    {                  
                        break;
                    }
                case PROTOCOL.OBJ_STATE:
                    {               
                        string json = msg.pop_string();                     

                        //다른이에겐 OTHER_PLAYER_WORLD_IN으로 대답 
                        CPacket respone = CPacket.create((short)PROTOCOL.OBJ_STATE);
                        respone.push(json);
                        WorldLogic.Instance.SendPacketNear(respone, this);
                        break;
                    }

				case PROTOCOL.CHAT_MSG_REQ:
					{
						string text = msg.pop_string();
						Console.WriteLine(string.Format("text {0}", text));

						CPacket response = CPacket.create((short)PROTOCOL.CHAT_MSG_ACK);
						response.push(text + "_rs");
						send(response);
                        break;
                    }

                case PROTOCOL.LOG_IN:
                    {
                        //2017 05 28
                        //login 성공 시 케릭터 정보를 보내야 합니다. 하지만 우선은 succes 또는 failed를 보냅니다
                        //케릭터 정보를 보내야 합니다.
                        int instanceId = msg.pop_int32();
                        string json = msg.pop_string();
                        var node = SimpleJSON.JSON.Parse(json);
                        string result = WorldLogic.Instance.LoginCheck(node["id"], node["pw"]) == true ? "succes" : "failed";
                        CPacket response = CPacket.create((short)PROTOCOL.LOG_IN);
                        response.push(instanceId);
                        response.push(result);
                        send(response);
                        break;
                    }

                case PROTOCOL.PLAYER_WORLD_IN:
                    {
                        int instanceId = msg.pop_int32();
                        string json = msg.pop_string();
                        Dictionary<string, string> jsonDic = new Dictionary<string, string>();
                        jsonDic.Add("id", currentId.ToString());
                        playerInstanceId = currentId;
                        string result2 = JsonConvert.SerializeObject(jsonDic);

                        string otherPlayers = "";
                        for(int i = 0; i < WorldLogic.Instance.userlist.Count;++i)
                        {
                            if (WorldLogic.Instance.userlist[i].playerInstanceId == playerInstanceId)
                                continue;

                            otherPlayers += WorldLogic.Instance.userlist[i].playerInstanceId;

                            if (i < WorldLogic.Instance.userlist.Count - 1)
                                otherPlayers += ",";
                        }
                        jsonDic.Add("other_Player", otherPlayers);

                        string result = JsonConvert.SerializeObject(jsonDic);

                        //나에겐 PLAYER_WORLD_IN 으로 대답
                        CPacket response = CPacket.create((short)PROTOCOL.PLAYER_WORLD_IN);
                        response.push(instanceId);
                        response.push(result);
                        send(response);

                        //다른이에겐 OTHER_PLAYER_WORLD_IN으로 대답 
                        CPacket response2 = CPacket.create((short)PROTOCOL.OTHER_PLAYER_WORLD_IN);
                        response2.push(result2);
                        WorldLogic.Instance.SendPacketNear(response2, this);
                        
                        currentId++;


                        IsInWorld = true;

                        break;
                    }
					
			}
		}

		void IPeer.on_removed()
		{
			Console.WriteLine("The client disconnected.");

			Program.remove_user(this);
		}

		public void send(CPacket msg)
		{
            if(IsCanSend)
                this.token.send(msg);
		}

		void IPeer.disconnect()
		{
			this.token.socket.Disconnect(false);
		}

		void IPeer.process_user_operation(CPacket msg)
		{
		}

        





    }
}